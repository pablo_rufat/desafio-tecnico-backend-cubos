# Processo Seletivo: Backend

Autor: Pablo Rufat Muñoz

## Endpoints:

### Home

localhost:3000/

    - Metodo: GET
    - Descrição: Home

### Documentação

localhost:3000/help

    - Metodo: GET
    - Descrição: Documentação sobre os endpoinds e tipos de dados.

### Listar regras

localhost:3000/cadastro

    - Metodo: GET
    - Descrição: Listado de regras de horários para atendimento.

### Cadastro de regra

localhost:3000/cadastro

    - Metodo: POST
    - Descrição: Cadastro de regra de atendimento. A nova regra de atendimento deverá ser construida no body da requisição conforme aos seguintes tipos.
    - Tipos de regras de atendimento:
    -       {'dia':'25-6-2018','from':'09:30','to':'10:20'}
    -       {'from':'9:30','to':'10:20'}
    -       {'dias_semana':['segunda','quarta'],'from':'09:30','to':'10:20'}
    - Formato de data : "DD-MM-YYYY"
    - Formato de hora" : "HH:mm"
    - Nomes dos dias aceitos: "segunda, terça, quarta, quinta, sexta"

### Apagar regra

localhost:3000/cadastro?id=X

    - Metodo: DELETE
    - Descrição: Apagar regra de horário para atendimento. Para apagar todos as regras de aendimento botar o id=-1.
    - parametros: "id",
    - exemplo: "localhost:3000/cadastro?id=1",

### Consultar horarios disponíveis

localhost:3000/consulta

    - Metodo: GET
    - Descrição: Retorna os horários disponíveis, baseado nas regras criadas anteriormente, considerando um intervalo de datas informadas na requisição.
    - parametros:
    -       "inicio" : "DD-MM-YYYY"
    -       "fim" : "DD-MM-YYYY"
    - exemplo: "localhost:3000/consulta?inicio=25-01-2018&fim=29-01-2018"

## Requisitos

    - node.js v8.11.3
    - npm 5.6.0

## Execução e Teste unitario

```bash
npm install
node index.js

npm test
```