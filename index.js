const express = require("express");
const fs = require("fs");

const app = express();
app.use(express.json());

const regras = fs.readFileSync('./database/regras.json');
const regrasJSON = JSON.parse(regras);
const doc = fs.readFileSync('./database/doc.json');
const docJSON = JSON.parse(doc);

app.listen(3000, () => {
    console.log("Servidor inizializado na porta 3000");
});

app.get('/', (req, res) => {

    let resposta = {
        mensagem: 'API REST para facilitar o gerenciamento de horários de uma clínica'
    }

    res.status(200).send(resposta);
});

//Documentação sobre os endpoinds e tipos de dados.
app.get('/help', (req, res) => {

    let resposta = {
        mensagem: 'Documentação',
        doc: docJSON
    }

    res.status(200).send(resposta);
});

app.route('/cadastro')
    //Cadastro de nova regra de atendimento
    .post((req, res) => {

        let resposta = {};
        var regra = req.body;

        if(regra.intervalos.length == 0){
            resposta = {
                mensagem: "insira um intervalo de tempo para a regra."
            }
            res.status(400).send(resposta);
        }else if(!checkFormat(regra)){
            resposta = {
                mensagem: "Formato de data ou hora invalidos."
            }
            res.status(400).send(resposta);
        }else if(existeConflito(regra)){
            resposta = {
                mensagem: "Conflito com as regras já existentes."
            }
            res.status(400).send(resposta);
        }else{
            //id numerico autoincrmental
            let id = Math.max.apply(Math, regrasJSON.map((r) => { return r.id; })) + 1;
            regra.id = id < 0 ? 0 : id;

            if(regra.dias_semana !== undefined)
                regra = diaSemanaToInt(regra);

            if(regra.dia !== undefined && regrasJSON.find(r => r.dia === regra.dia)){
                regrasJSON.find(r => r.dia === regra.dia).intervalos.push(...regra.intervalos);
            }else if(regra.dias_semana !== undefined && regrasJSON.find(r => JSON.stringify(r.dias_semana) === JSON.stringify(regra.dias_semana))){
                regrasJSON.find(r => JSON.stringify(r.dias_semana) === JSON.stringify(regra.dias_semana)).intervalos.push(...regra.intervalos);
            }else if(regra.dia === undefined && regra.dias_semana === undefined && regrasJSON.find(r => r.dia === undefined && r.dias_semana === undefined)){
                regrasJSON.find(r => r.dia === undefined && r.dias_semana === undefined).intervalos.push(...regra.intervalos);
            }else{
                regrasJSON.push(regra);
            }

            fs.writeFile('./database/regras.json', JSON.stringify(regrasJSON), (err) => {
                if (err){
                    resposta = {
                        mensagem: err
                    }
                    res.status(500).send(resposta);
                }else{
                    resposta = {
                        mensagem: "Regra cadastrada com sucesso."
                    }
                    res.status(200).send(resposta);
                }
            });
        }
    })
    //Listar regras
    .get((req, res) => {
        res.status(200).send(regrasJSON);
    })
    //Apagar regra
    .delete((req, res) => {
        let removeId = req.query.id;

        if(removeId == -1){
            regrasJSON.splice(0,regrasJSON.length);
            fs.writeFile('./database/regras.json', JSON.stringify(regrasJSON), (err) => {
                if (err){
                    resposta = {
                        mensagem: err
                    }
                    res.status(500).send(resposta);
                }else{
                    resposta = {
                        mensagem: "Regras apagadas com sucesso."
                    }
                    res.status(200).send(resposta);
                }
            });
        }else{

            let removeRegra = regrasJSON.find((r) => r.id === Number(removeId));
            var resposta = {};

            if(removeRegra !== undefined){
                regrasJSON.splice(regrasJSON.indexOf(removeRegra), 1);
                fs.writeFile('./database/regras.json', JSON.stringify(regrasJSON), (err) => {
                    if (err){
                        resposta = {
                            mensagem: err
                        }
                        res.status(500).send(resposta);
                    }else{
                        resposta = {
                            mensagem: "Regra apagada com sucesso."
                        }
                        res.status(200).send(resposta);
                    }
                });
            }else{
                resposta = {
                    mensagem: `A regra com id ${removeId} não existe.`
                }
                res.status(400).send(resposta);
            }
        }
    });

//TODO - Horários disponíveis
app.get('/consulta', (req, res) => {
    let inicio = req.query.inicio.split('-');
    let fim = req.query.fim.split('-');
    let dataInicial = new Date(inicio[2],inicio[1]-1,inicio[0]);
    let dataFinal = new Date(fim[2],fim[1]-1,fim[0]);

    res.status(200).send(getRegras(dataInicial, dataFinal));
});

//Erro 404
app.use((req, res, next) => {
    resposta = {
        mensaje: 'URL não encontrada'
    };
    res.status(404).send(resposta);
});

//Função para descobrir se existe algum conflito de horarios com a nova regra.
function existeConflito(novaRegra){
    let conflito = false;

    for(let regra of regrasJSON){

        if(conflito === true) break;

        if(novaRegra.dia !== undefined && novaRegra.dia === regra.dia){
            for(let i of novaRegra.intervalos){
                if(regra.intervalos.find(intervalo => 
                    horaNumber(intervalo.from) < horaNumber(i.to) && horaNumber(intervalo.to) > horaNumber(i.to) || 
                    horaNumber(intervalo.to) > horaNumber(i.from) && horaNumber(intervalo.from) < horaNumber(i.from) ||
                    horaNumber(intervalo.to) === horaNumber(i.to) && horaNumber(intervalo.from) === horaNumber(i.from)))
                    conflito = true;
            }
        }else if(novaRegra.dias_semana !== undefined && regra.dias_semana !== undefined){
            for(let dia of novaRegra.dias_semana){
                if(regra.dias_semana.find(ds => dia === ds.nome)){
                    for(let i of novaRegra.intervalos){
                        if(regra.intervalos.find(intervalo => 
                            horaNumber(intervalo.from) < horaNumber(i.to) && horaNumber(intervalo.to) > horaNumber(i.to) || 
                            horaNumber(intervalo.to) > horaNumber(i.from) && horaNumber(intervalo.from) < horaNumber(i.from) ||
                            horaNumber(intervalo.to) === horaNumber(i.to) && horaNumber(intervalo.from) === horaNumber(i.from)))
                            conflito = true;
                    }
                }
            }
        }else if(novaRegra.dias_semana === undefined && novaRegra.dia === undefined){
            for(let i of novaRegra.intervalos){
                if(regra.intervalos.find(intervalo => 
                    horaNumber(intervalo.from) < horaNumber(i.to) && horaNumber(intervalo.to) > horaNumber(i.to) || 
                    horaNumber(intervalo.to) > horaNumber(i.from) && horaNumber(intervalo.from) < horaNumber(i.from) ||
                    horaNumber(intervalo.to) === horaNumber(i.to) && horaNumber(intervalo.from) === horaNumber(i.from)))
                    conflito = true;
            }
        }
    }

    return conflito;
}

// Função que adiciona um valor numerico a cada dia da semana.
function diaSemanaToInt(regra){
    var diasSemana = [
        {nome:'segunda', dia: 1},
        {nome:'terça', dia: 2},
        {nome:'quarta', dia: 3},
        {nome:'quinta', dia: 4},
        {nome:'sexta', dia: 5}
    ];

    newDiasSemana = [];
    for(let dia of regra.dias_semana){
        newDiasSemana.push(diasSemana.find(d => d.nome === dia));
    }
    regra.dias_semana = newDiasSemana;

    return regra;
}

//Função que comprova que os formatos de data e hora estão certos
//Expresões regulares obtidas de http://regexlib.com/
function checkFormat(regra){
    var diaRegExp = /^(((((0[1-9])|(1\d)|(2[0-8]))-((0[1-9])|(1[0-2])))|((31-((0[13578])|(1[02])))|((29|30)-((0[1,3-9])|(1[0-2])))))-((20[0-9][0-9]))|(29-02-20(([02468][048])|([13579][26]))))$/;
    var diasSemana = ['segunda','terça','quarta','quinta','sexta'];
    var horaRegExp = /^([0-1][0-9]|[2][0-3]):([0-5][0-9])$/;
    var valido = true;

    if(regra.dia){
        valido = diaRegExp.test(regra.dia);
    }else if(regra.dias_semana){
        for(let dia of regra.dias_semana){
            valido = diasSemana.includes(dia);
            if(!valido) break;
        }
    }

    for(let intervalo of regra.intervalos){
        
        valido = horaRegExp.test(intervalo.from);
        if(!valido) break;
        valido = horaRegExp.test(intervalo.to);
        if(!valido) break;
    }

    return valido;
}

//Função para a comprovação de horarios disponiveis
function getRegras(dataInicial, dataFinal){

    let horarios = new Array();

    let auxData = dataInicial;
    while(auxData.getDate() !== dataFinal.getDate()+1){
        for(let regra of regrasJSON){
            if(regra.dia !== undefined){                
                let dia = regra.dia.split('-');
                let diaData = new Date(dia[2],dia[1]-1,dia[0]);
                    if(diaData.getDate() === auxData.getDate()){  
                        if(!horarios.find(h => h.day === regra.dia)){
                            horarios.push({
                                day: regra.dia,
                                intervals: new Array(...regra.intervalos)
                            });   
                        }else{
                            horarios.find(h => h.day === regra.dia).intervals.push(...regra.intervalos);
                        }                 
                    }
            }
            else if(regra.dias_semana !== undefined){
                for(let diaSemana of regra.dias_semana){
                    if(auxData.getDay() === diaSemana.dia){
                        let horario = horarios.find(h => h.day === `${auxData.getDate()}-${auxData.getMonth()+1}-${auxData.getFullYear()}`);                      
                        if(!horario){
                            horarios.push({
                                day: `${auxData.getDate()}-${auxData.getMonth()+1}-${auxData.getFullYear()}`,
                                intervals: new Array(...regra.intervalos)
                            }); 
                        }else{
                            horario.intervals.push(...regra.intervalos);
                        }
                    }
                }
            }
            else{
                if(!horarios.find(h => h.day === `${auxData.getDate()}-${auxData.getMonth()+1}-${auxData.getFullYear()}`)){
                    horarios.push({
                        day: `${auxData.getDate()}-${auxData.getMonth()+1}-${auxData.getFullYear()}`,
                        intervals: new Array(...regra.intervalos)
                    });
                }else{                    
                    horarios.find(h => h.day === `${auxData.getDate()}-${auxData.getMonth()+1}-${auxData.getFullYear()}`).intervals.push(...regra.intervalos);
                }
            }
        }      
        auxData.setDate(auxData.getDate() + 1);
    }

    return horarios;
}

//função que transforma as horas em numeros para facilitar a comparação.
function horaNumber(hora){
    let aux = hora.replace(':','');
    return Number(aux);
}