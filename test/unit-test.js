const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = require('chai').expect;
chai.use(chaiHttp);
const url= 'http://localhost:3000';

const fs = require("fs");

const doc = fs.readFileSync('./database/doc.json');
const docJSON = JSON.parse(doc);


describe ('Home page', () => {
    it('Request na raiz.', (done) => {
        chai.request(url)
        .get('/')
        .end((err, res) => {
            console.log(res);        
            expect(res.statusCode).to.equal(200);
            done();
        });
    });
});

describe ('Help page', () => {
    it('Request na rota /help', (done) => {
        chai.request(url)
        .get('/help')
        .end((err, res) => {
            console.log(res);        
            expect(res.statusCode).to.equal(200);
            expect(res.body).to.have.property('mensagem').to.be.equal('Documentação');
            done();
        });
    });
});

describe ('Cadastro page', () => {
    it('Request esvaziar JSON para começar testes', (done) => {
        chai.request(url)
        .delete('/cadastro?id=-1')
        .end((err, res) => {
            console.log(res);        
            expect(res.statusCode).to.equal(200);
            done();
        });
    });
    it('Request criar regra para todos os dias a primeira vez', (done) => {
        chai.request(url)
        .post('/cadastro')
        .send({
            intervalos : [
                {
                    "from":"22:00",
                    "to":"23:00"
                }
            ]
         })
        .end((err, res) => {
            console.log(res);        
            expect(res.statusCode).to.equal(200);
            done();
        });
    });
    it('Request criar regra para todos os dias que já existe', (done) => {
        chai.request(url)
        .post('/cadastro')
        .send({
            intervalos : [
                {
                    "from":"22:00",
                    "to":"23:00"
                }
            ]
         })
        .end((err, res) => {
            console.log(res);        
            expect(res.statusCode).to.equal(400);
            done();
        });
    });
    it('Request criar regra para um dia', (done) => {
        chai.request(url)
        .post('/cadastro')
        .send({
            "dia":"25-1-2018",
            "intervalos" : [
                 {
                     "from":"13:30",
                     "to":"14:30"
                 },
                 {
                     "from":"15:00",
                     "to":"16:00"
                 }
            ]
         })
        .end((err, res) => {
            console.log(res);        
            expect(res.statusCode).to.equal(200);
            done();
        });
    });
    it('Request criar regra semanal', (done) => {
        chai.request(url)
        .post('/cadastro')
        .send({
            "dias_semana":
            [
                "segunda",
                "quarta"
            ],
            "intervalos":
            [
                {
                    "from":"20:30",
                    "to":"21:30"
                    
                }
            ]
        })
        .end((err, res) => {
            console.log(res);        
            expect(res.statusCode).to.equal(200);
            done();
        });
    });
    it('Request get Regras', (done) => {
        chai.request(url)
        .get('/cadastro')
        .end((err, res) => {
            console.log(res);        
            expect(res.statusCode).to.equal(200);
            expect(res.body).to.be.an('array');
            done();
        });
    });
});

describe ('Consultas', () => {
    
    it('Request get Regras', (done) => {
        chai.request(url)
        .get('/consulta?inicio=25-01-2018&fim=29-01-2018')
        .end((err, res) => {
            console.log(res);        
            expect(res.statusCode).to.equal(200);
            expect(res.body).to.be.an('array');
            done();
        });
    });
});